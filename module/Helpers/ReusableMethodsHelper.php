<?php

namespace Module\Helpers;

class ReusableMethodsHelper{

    public function http_url_request_builder($parameters){

    	return urldecode(http_build_query($parameters));
    }

    public function random_number(){

        return mt_rand(100000,999990);
    }

    public function randomString($l = 8) {
        
        return substr(md5(uniqid(mt_rand(), true)), 0, $l);
    }

}


?>