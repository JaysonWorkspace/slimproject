<?php

namespace Module\Helpers;

use Module\Services\REST;

class MedooHelper extends SettingsHelper{

	public function __construct($c){

		parent::__construct($c);

		$this->REST	         			= new REST();
		$this->Tool 		 			= new \Tool();
		$this->ResponseCodes 			= new \ResponseCodes();
		$this->ReusableMethodsHelper	= new ReusableMethodsHelper();
	}

	public function insert($database, $table_name, $data){
		
		$database->insert($table_name, $data);

		return $database->id();
	}

	public function formatWhere($where){

		if(!is_array($where)){
			$where = [];
		}

		if(count($where) > 1){
			
			$temp = [];
	        $temp['AND'] = $where;
	       	$where = $temp;
		}

		return $where;
	}

	public function delete($database, $table, $where){	
		
		$where = $this->formatWhere($where);

		if($where == 0){
			$where = [];
		}

		$result = $database->delete($table, $where);
		
		return $result->rowCount();
	}

	public function update($database, $table, $data, $where){	
		
		$where = $this->formatWhere($where);

		if($where == 0){
			$where = [];
		}

		$result = $database->update($table, $data, $where);

		return $result->rowCount();
	}

	public function select($database, $table_name, $join, $column, $where){

		$where = $this->formatWhere($where);

		if($column == ""){

			$column = "*";

		}

		if($where == 0){
			$where = [];
		}	

		if($join != 0){

			$result = $database->select($table_name, $join, $column, $where);

		} else {

			$result = $database->select($table_name, $column, $where);
			
		}

		return $result;
	}


	public function get($database, $table_name, $join, $data, $where){

		$where = $this->formatWhere($where);

		if($data == ""){
			$data = "*";
		}

		if($where == 0){
			$where = [];
		}
			
		if($join != 0){

			$result = $database->get($table_name, $join, $data, $where);
			
		} else {

			$result = $database->get($table_name, $data, $where);
		}

		return $result;
	}

	public function startTransaction($database){

		return $database->pdo->beginTransaction();
	}

	public function rollBack($database){

		return $database->pdo->rollBack();
	}

	public function commit($database){

		return $database->pdo->commit();
	}


}