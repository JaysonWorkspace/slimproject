<?php 

namespace Module\Handlers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Exception;

class ExceptionErrorHandler{	
	
	protected $logger;
	
	public function __construct(LoggerInterface $logger){

		$this->logger = $logger;
	}


	public function __invoke(Request $request, Response $response, Exception $exception){

		$message = [];
		$message['message']	= $exception->getMessage();
		$message['line']	= $exception->getLine();
		$message['file']	= $exception->getFile();
		$message['status']	= $exception->getCode() == 0 ? 500 : $exception->getCode();
		$message['code']    = $message['status'];

		$this->logger->critical($exception->getMessage());
     
        return $response->withJSON($message, $message['status']);

	}

}

