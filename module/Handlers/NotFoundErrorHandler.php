<?php  
 
namespace Module\Handlers; 
 
use Psr\Http\Message\ServerRequestInterface as Request; 
use Psr\Http\Message\ResponseInterface as Response; 
use Psr\Log\LoggerInterface; 
 
 
class NotFoundErrorHandler {   

    protected $logger; 
   
    public function __construct(LoggerInterface $logger) { 

        $this->logger = $logger; 
    } 
 
 
    public function __invoke(Request $request, Response $response){ 
 
        $str_message = "Page Not Found"; 

        $message = [];
        $message['message'] = $str_message;
        $message['status']  = 404;
        $message['code']    = $message['status'];
 
        $this->logger->critical($str_message); 
 
        return $response->withJSON($message, $message['status']); 
    } 
 
}