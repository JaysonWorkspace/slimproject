<?php 

namespace Module\Controllers;

class UserController{

    private $UserModel;

    public function __construct($c){

        $this->UserModel = new \Module\Models\UserModel($c);
    }

    public function getOne($req, $res, $arg){

        $user_id = $req->getAttribute('user_id');

        $validation_rules = [
            'user_id'    => 'required|numeric'
        ];

        $parameters['user_id'] = $user_id;

        $this->UserModel->validator->validation_rules($validation_rules);

        $isValid = $this->UserModel->validator->run($parameters);

        if($isValid === false){

            $message = $this->UserModel->validator->get_errors_array();

            return $res->withJSON($message, 400);
        }

        $result = $this->UserModel->getone($parameters);

        if(!$result){

            $message = $this->UserModel->ResponseCodes->get(0, 1010);

            return $res->withJSON($message, 400);
        }

        return $res->withJSON($result, 200);
    }

    public function getUser($req, $res, $arg){

        $user_id = $req->getAttribute('user_id');

        $validation_rules = [
            'user_id'    => 'required|numeric'
        ];

        $parameters['user_id'] = $user_id;

        $this->UserModel->validator->validation_rules($validation_rules);

        $isValid = $this->UserModel->validator->run($parameters);

        if($isValid === false){

            $message = $this->UserModel->validator->get_errors_array();

            return $res->withJSON($message, 400);
        }

        $result = $this->UserModel->getUser($parameters);

        if(!$result){

            $message = $this->UserModel->ResponseCodes->get(0, 1010);

            return $res->withJSON($message, 400);
        }

        return $res->withJSON($result, 200);
    }

    public function getUsers($req, $res, $arg){

        $result = $this->UserModel->getUsers();

        if(!$result){

            $message = $this->UserModel->ResponseCodes->get(0, 1010);

            return $res->withJSON($message, 400);
        }

        return $res->withJSON($result, 200);
    }

    public function addUser($req, $res, $arg){

        $parameters = $req->getParsedBody();

        if(!$parameters){
            $parameters = [];
        }

        $validation_rules = [
            'username'  => 'required',
            'password'  => 'required',
            'firstname' => 'required',
            'lastname'  => 'required',
            'type'      => 'required'
        ];

        $isValid = $this->UserModel->validator->validate($parameters, $validation_rules);

        if($isValid !== true){

            $message = $this->UserModel->validator->get_errors_array();

            return $res->withJSON($message, 400);
        }

        $username['username'] = $parameters['username'];

        $check_username = $this->UserModel->checkUsername($username);

        if($check_username){

            $message = $this->UserModel->ResponseCodes->get(0, 1011);

            return $res->withJSON($message, 400);
        }

        $result = $this->UserModel->addUser($parameters);

        if(!$result){

            $message = $this->UserModel->ResponseCodes->get(0, 1006);
            
            return $res->withJSON($message, 400);
        }

        $message = $this->UserModel->ResponseCodes->get(1, 2000);

        return $res->withJSON($message, 200);
    }

    public function updateUser($req, $res, $arg){

        $parameters = $req->getParsedBody();

        if(!$parameters){
            $parameters = [];
        }

        $validation_rules = [
                        'user_id'   => 'required|numeric'
                    ];

        $isValid = $this->UserModel->validator->validate($parameters, $validation_rules);

        if($isValid !== true){

            $message = $this->UserModel->validator->get_errors_array();

            return $res->withJSON($message, 400);
        }

        $result = $this->UserModel->updateUser($parameters);

        if($result === 0){

            $message = $this->UserModel->ResponseCodes->get(0, 1008);
            
            return $res->withJSON($message, 400);
        }
        
        if($result === false){

            $message = $this->UserModel->ResponseCodes->get(0, 1007);
            
            return $res->withJSON($message, 400);
        }

        $message = $this->UserModel->ResponseCodes->get(1, 2001);

        return $res->withJSON($message, 200);
    }

    public function deleteUser($req, $res, $arg){

        $user_id = $req->getAttribute('user_id');

        $validation_rules = [
            'user_id'    => 'required|numeric'
        ];

        $parameters['user_id'] = $user_id;

        $isValid = $this->UserModel->validator->validate($parameters, $validation_rules);

        if($isValid !== true){

            $message = $this->UserModel->validator->get_errors_array();

            return $res->withJSON($message, 400);
        }

        $result = $this->UserModel->deleteUser($parameters);

        if(!$result){

            $message = $this->UserModel->ResponseCodes->get(0, 1009);
            
            return $res->withJSON($message, 400);
        }

        $message = $this->UserModel->ResponseCodes->get(1, 2002);

        return $res->withJSON($message, 200);
    }

}