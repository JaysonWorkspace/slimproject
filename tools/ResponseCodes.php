<?php

Class ResponseCodes{

	protected static $Error = [

		1000 => 'Unknown Error',
		1001 => 'Unexpected Error',
		1002 => 'Invalid Token',
		1003 => 'Unauthorized Actions',
		1004 => 'Invalid Username or Password',
		1005 => 'Deactivated Account, Please Contact Your Administrator',
		1006 => 'Unable to add',
		1007 => 'Unable to update',
		1008 => 'Nothing to update',
		1009 => 'Unable to delete',
		1010 => 'No Results Found',
		1011 => 'Username already exist',

	];

	protected static $Success = [

		2000 => 'Successfully Added',
		2001 => 'Successfully Updated',
		2002 => 'Successfully Deleted'

	];

	public static function get(int $type, $code){

		$result = [];

		$getCodes = [];

		switch ($type) {
		    case 0:
		    	$getCodes = self::$Error;
		        break;
		    case 1:
		    	$getCodes = self::$Success;
		        break;
		    default:
		    	$getCodes = [];
		}

		if(empty($getCodes)){

			$result['code']    = '00000';
			$result['message'] = 'Message type id '.$type. ' is invalid';

			return $result;
		}

		foreach ($getCodes as $key => $value) {
			
			if($key == $code){

				$result['code'] = $key;
				$result['message'] = $value;
				return $result;
				
			}
		}

		$result['code']    = '00001';
		$result['message'] = 'Message code id '.$code. ' is invalid';

		return $result;
	}
	

}