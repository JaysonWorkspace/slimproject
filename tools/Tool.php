<?php

class Tool {

    //encrypt or decrypt string or array values
    public function encrypt_decrypt(String $action, $data){

        switch ($action) {

            case 'encrypt':
                
                //$data = $this->array_encode($data);
                $cipher_method = 'aes-128-ctr';
                $enc_key = openssl_digest(php_uname(), 'SHA256', TRUE);
                $enc_iv  = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher_method));
                $crypted_token = openssl_encrypt($data, $cipher_method, $enc_key, 0, $enc_iv) . "::" . bin2hex($enc_iv);

                return $crypted_token;

                break;

            case 'decrypt':
                
                list($data, $enc_iv) = explode("::", $data);;
                $cipher_method = 'aes-128-ctr';
                $enc_key = openssl_digest(php_uname(), 'SHA256', TRUE);
                $token = openssl_decrypt($data, $cipher_method, $enc_key, 0, hex2bin($enc_iv));

                //$token = $this->array_decode($token);

                return $token;
                
                break;

            default:

                return 'Action must be either encrypt or decrypt.';

                break;
        }
    }

    public function array_encode($data){

        return base64_encode(serialize($data));
    }

    public function array_decode($data){

        return unserialize(base64_decode($data));
    }

    public function match_encrypted($string, $encrypted) {

        return ($this->encrypt_decrypt('decrypt', $encrypted) == $string ? true : false);
    }

    public function generateToken(){

        return md5(uniqid(rand(), true));
    }

    public function randomString($l = 8){

        return substr(md5(uniqid(mt_rand(), true)), 0, $l);
    } 

}