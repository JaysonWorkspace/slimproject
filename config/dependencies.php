<?php

class DI{
    
    public $Container;
    public $Settings;
    public $C;

    public function __construct($c){

        $this->C         = $c;
        $this->Settings  = $c->get('settings');
        $this->Container = $this->getContainer();
    }

    public function __invoke() { 

        return $this->Container;

    } 

    public function getContainer(){

        return [
            'template'      => $this->template(),
            'flash'         => $this->flash(),
            'validator'     => $this->gump(),
            'logger'        => $this->logger(),
            'database'      => $this->database()
        ];

    }

    public function template(){

        $view = new \Slim\Views\Twig($this->Settings['view']['template_path'], $this->Settings['view']['twig']);
        $view->addExtension(new Slim\Views\TwigExtension($this->C->get('router'), $this->C->get('request')->getUri()));
        $view->addExtension(new Twig_Extension_Debug());

        return $view;
    }

    public function flash(){

        return new Slim\Flash\Messages;
    }

    public function gump(){

        return new GUMP;
    }

    public function logger(){

        $logger = new Monolog\Logger($this->Settings['logger']['name']);
        $logger->pushProcessor(new Monolog\Processor\UidProcessor());
        $logger->pushHandler(new Monolog\Handler\StreamHandler($this->Settings['logger']['path'], \Monolog\Logger::DEBUG));

        $this->C['logger'] = $logger;

        return $logger;
    }

    public function database(){

        $database = $_SERVER['SERVER_NAME'] == 'localhost' ? $this->Settings['localhost'] : $this->Settings['database'];

        return new Medoo\Medoo($database); 
    }

}

