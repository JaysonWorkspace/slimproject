<?php
// Routes

$app->group('/user', function () use ($app) {
	
	$app->get("/getone/{user_id}", 		"Module\Controllers\UserController:getOne");
	$app->get("/getuser/{user_id}", 	"Module\Controllers\UserController:getUser");
	$app->get("/getusers", 				"Module\Controllers\UserController:getUsers");
	$app->post("/adduser", 				"Module\Controllers\UserController:addUser");
	$app->post("/updateuser", 			"Module\Controllers\UserController:updateUser");
	$app->post("/deleteuser/{user_id}",	"Module\Controllers\UserController:deleteUser");
}); 



